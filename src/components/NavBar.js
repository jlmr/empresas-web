import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { withStyles, Toolbar, Grid, Button } from '@material-ui/core'
import LogoNav from '../Assets/logo-nav.png'
import LogoSearch from '../Assets/ic-search-copy.svg'
import SearchIcon from '@material-ui/icons/Search'
import TextInput from './TextInput'
import { searchEnterprises } from '../actions/enterprises'
import _ from 'lodash';

const styles = () => ({
  navBar: {
    background: '#ee4c77',
    height: 151
  },

  logoNav: {
    objectFit: 'contain'
  },

  icSearch: {
    bjectFit: 'contain'
  },

  icClose: {
    width: 60,
    height: 60,
    objectFit: 'contain'
  }
})

class NavBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: false,
      searchText: ''
    };

    this.handleSearch = _.debounce(this.handleSearch, 500);
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
    this.handleSearch();
  }

  handleSearch = () =>
    this.props.searchEnterprises(this.state.searchText, this.props.auth);

  render() {
    const { classes, history } = this.props
    return (
      <>
        {
          history.location.pathname !== '/' &&
          <Toolbar className={classes.navBar} >
            {
              !this.state.search && <Grid container
                className={classes.logoNav}
                justify='center'
                alignContent='center'
                direction='row'>
                <img className={classes.logoNAv} src={LogoNav} />
              </Grid>
            }
            {
              this.state.search &&
              <TextInput
                value={this.state.searchText}
                onChange={this.handleChange}
                name={'searchText'}
                id={'searchText'}
                label={'Pesquisar'}
                icon={<SearchIcon />} />
            }
            <Grid container
              className={classes.logoNav}
              justify='center'
              alignContent='right'
              direction='row'>
              <Button onClick={() => this.setState(prevState => ({ search: !prevState.search }))}>
                <img className={classes.icSearch} src={LogoSearch} />
              </Button>
            </Grid>
          </Toolbar>
        }
      </>
    )
  }
}

function mapStateToProps({ auth }) {
  return {
    auth
  }
}

export default connect(mapStateToProps, { searchEnterprises })(withStyles(styles)(withRouter(NavBar)))
