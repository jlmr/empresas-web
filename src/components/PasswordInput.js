import React, { useState } from 'react';
import TextInput from './TextInput';
import { InputAdornment, IconButton } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';

const PasswordInput = (props) => {
  const [showPwd, setShowPwd] = useState(false);

  return <TextInput
    {...props}
    type={showPwd ? "text" : "password"}
    endAdornment={
      <InputAdornment position="end">
          <IconButton
              aria-label="toggle password visibility"
              onClick={() => setShowPwd(!showPwd)}
              onMouseDown={e => e.preventDefault()}>
              {showPwd ? <Visibility /> : <VisibilityOff />}
          </IconButton>
      </InputAdornment>
  } />
}

export default PasswordInput;