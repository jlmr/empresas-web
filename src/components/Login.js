import React, { Component } from 'react';
import { connect } from 'react-redux';
import { authenticateUser } from '../actions/auth';
import { withRouter } from 'react-router-dom';
import { withStyles, Grid, Typography, Button } from '@material-ui/core';
import MailOutline from '@material-ui/icons/MailOutline';
import LockOpenOutlined from '@material-ui/icons/LockOpenOutlined';
import Logo from '../Assets/logo-home.png';
import TextInput from './TextInput';
import PasswordInput from './PasswordInput';

const styles = () => ({
  content: {
    paddingTop: '3em'
  },

  logo: {
    height: 72,
    objectFit: 'contain'
  },

  welcome: {
    marginTop: 57,
    fontSize: 24,
    fontWeight: 'bold',
    fontStyle: 'normal'
  },

  lorem: {
    marginTop: 20.5,
    fontSize: 18,
    fontWeight: 'normal',
    fontStyle: 'normal'
  },

  formContent: {
    marginTop: 40,
  },

  marginInput: {
    marginTop: 26
  },

  button: {
    marginTop: 40,
    background: '#57bbbc',
    marginLeft: 20,
    marginRight: 20
  },

  btnLabel: {
    color: '#FFF',
    fontWeight: 'bold'
  },

  errorMsg: {
    color: "#F44336",
    fontSize: 15,
    marginTop: 10
  }
})

class Login extends Component {
  state = {
    email: '',
    password: ''
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth) {
      this.props.history.push("/home");
      return;
    }
  }

  onChange = e => this.setState({
    [e.target.name]: e.target.value,
  })

  isFormValid = () => this.state.email && this.state.password

  onLogin = () => {
    const body = {
      email: this.state.email,
      password: this.state.password
    }

    this.props.authenticateUser(body);
  }

  render() {
    const { classes, hasErrors, errors } = this.props

    return (
      <Grid
        container
        className={classes.content}
        justify='center'
        alignContent='center'
        direction='column'>
        <img className={classes.logo} src={Logo} />
        <Typography align='center' className={classes.welcome}>
          BEM-VINDO AO<br />EMPRESAS
        </Typography>
        <Typography align='center' className={classes.lorem}>
          Lorem ipsum dolor sit amet, contetur<br />adipiscing elit. Nunc accumsan.
        </Typography>
        <Grid item className={classes.formContent}>
          <TextInput
            error={hasErrors}
            value={this.state.email}
            onChange={this.onChange}
            name={'email'}
            id={'email'}
            label={'E-mail'}
            icon={<MailOutline />} />
          <PasswordInput
            error={hasErrors}
            customClass={classes.marginInput}
            value={this.state.password}
            onChange={this.onChange}
            name={'password'}
            id={'password'}
            label={'Senha'}
            icon={<LockOpenOutlined />} />
        </Grid>
        {
          hasErrors &&
          <Typography
            align={"center"}
            className={classes.errorMsg}>
            {errors[0]}
          </Typography>
        }
        <Button
          variant='contained'
          onClick={this.onLogin}
          className={classes.button}
          disabled={!this.isFormValid()}
          classes={{
            label: classes.btnLabel
          }}>
          ENTRAR
        </Button>
      </Grid>
    )
  }
}

function mapStateToProps({ auth, errors }) {
  return {
    auth,
    hasErrors: errors.errorList.length > 0,
    errors: errors.errorList
  }
}

export default connect(mapStateToProps, { authenticateUser })(withStyles(styles)(withRouter(Login)))