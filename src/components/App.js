import React from 'react'
import { Provider } from "react-redux";
import store from "../store";
import { CssBaseline, createMuiTheme, MuiThemeProvider } from '@material-ui/core'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Login from './Login'
import Home from './Home'
import NavBar from './NavBar'
import Loading from './Loading';
import Enterprise from './Enterprise';

const theme = createMuiTheme({
  palette: {
    background: {
      default: '#ebe9d7'
    }
  }
})

const App = () => (
  <Provider store={store}>
    <Router>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Loading/>
        <NavBar />
        <Route exact path='/' component={Login}></Route>
        <Route exact path='/home' component={Home}></Route>
        <Route exact path='/enterprise' component={Enterprise}></Route>
      </MuiThemeProvider>
    </Router>
  </Provider>
)

export default App