import React, { Fragment } from 'react'
import { connect } from 'react-redux';
import { withStyles, CircularProgress } from '@material-ui/core'

const styles = () => ({
    bkg: {
        position: "absolute",
        width: "100%",
        height: "100%",
        zIndex: 1000,
        margin: 0,
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    bkgOpacity: {
        width: "100%",
        height: "100%",
        backgroundColor: "#FFF",
        opacity: 0.5
    },
    rootProgress: {
        position: "absolute"
    }
});

const Loading = ({ classes, isLoading }) => (
    <Fragment>
        {
            isLoading &&
            <div className={classes.bkg}>
                <div className={classes.bkgOpacity} />
                <CircularProgress classes={{ root: classes.rootProgress }} />
            </div>
        }
    </Fragment>
);

function mapStateToProps({ ui }) {
    return {
        isLoading: ui.isLoading
    }
}

export default connect(mapStateToProps)(withStyles(styles)(Loading));