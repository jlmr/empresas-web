import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import { withStyles, Typography, Grid } from '@material-ui/core'
import EnterpriseListItem from './EnterpriseListItem';

const styles = () => ({
  text: {
    marginTop: 359,
    fontSize: 32,
    textAlign: 'center'
  },
  container: {
    paddingTop: 35,
  }
})

const Home = ({ classes, enterprises }) => (
  <Grid
    container
    className={classes.container}
    alignContent='center'
    direction='column'>
    {
      enterprises.length > 0
        ? enterprises.map(enterprise => <EnterpriseListItem data={enterprise} />)
        : <Typography align='center' className={classes.text}>Clique na busca para iniciar.</Typography>
    }
  </Grid>
);

function mapStateToProps({ enterprises }) {
  return {
    enterprises: enterprises.list
  }
}

export default connect(mapStateToProps)(withStyles(styles)(withRouter(Home)))