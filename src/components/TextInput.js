import React from 'react';
import {
  withStyles,
  FormControl,
  InputLabel,
  Input,
  InputAdornment
} from '@material-ui/core';
import clsx from 'clsx';

const styles = () => ({
  inputIcon: {
    color: "#ee4c77"
  },
  formContent: {
    marginTop: 40
  }
});

const TextInput = ({ classes, customClass, label, id, name, value, onChange, type, icon, error, endAdornment }) => (
  <FormControl fullWidth className={clsx(customClass)}>
    <InputLabel
      error={error}
      htmlFor={id}>
      {label}
    </InputLabel>
    <Input
      error={error}
      type={type ? type : "text"}
      name={name}
      id={id}
      value={value}
      onChange={onChange}
      startAdornment={
        icon && <InputAdornment position="start" className={classes.inputIcon}>
          {icon}
        </InputAdornment>
      }
      endAdornment={endAdornment} />
  </FormControl>
);

export default withStyles(styles)(TextInput);