import React from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles, Typography, Grid } from '@material-ui/core'
import ImgEmpresaLista from '../Assets/img-e-1-lista.png';

const styles = () => ({
  item: {
      marginTop: 15,
      backgroundColor: "#FFF"
  },
  image: {
      margin: 25,
      backgroundImage: `url(${ImgEmpresaLista})`,
      height: 90
  }
})

const EnterpriseListItem = ({ classes, data, history }) => (
  <Grid
    container
    item
    xs={6}
    className={classes.item}>
    <Grid
        item
        xs={3}
        className={classes.image}>
    </Grid>
    <Grid
      container
      justify="center"
      direction="column"
      item
      xs={3}
      onClick={() => {
        history.push("/enterprise");
      }}
      >
      <Typography>{data.enterprise_name}</Typography>
      <Typography>{data.enterprise_type.enterprise_type_name}</Typography>
      <Typography>{data.country}</Typography>
    </Grid>
  </Grid>
)

export default (withStyles(styles)(withRouter(EnterpriseListItem)))