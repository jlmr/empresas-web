import {
  GET_ERRORS,
  CLEAR_ERRORS
} from "../actions/types";

const initialState = {
  errorList: []
};

export default function (state = initialState, action) {
  switch (action.type) {
      case GET_ERRORS:
          return {
              errorList: action.payload
          };

      case CLEAR_ERRORS:
          return initialState;

      default:
          return state;
  }
}