import { combineReducers } from "redux";
import auth from './auth';
import user from './user';
import enterprises from './enterprises';
import errors from "./errors";
import ui from './ui';

export default combineReducers({
    auth,
    user,
    enterprises,
    errors,
    ui
});