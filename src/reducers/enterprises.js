import {
  GET_ENTERPRISES
} from "../actions/types";

const initialState = {
  list: []
};

export default function (state = initialState, action) {
  switch (action.type) {
      case GET_ENTERPRISES:
          return {
              list: action.payload
          };

      default:
          return state;
  }
}