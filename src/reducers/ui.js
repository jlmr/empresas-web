import {
  TOGGLE_LOADING
} from "../actions/types";

const initialState = {
  isLoading: false
};

export default function (state = initialState, action) {
  switch (action.type) {
      case TOGGLE_LOADING:
          return {
              isLoading: !state.isLoading
          };

      default:
          return state;
  }
}