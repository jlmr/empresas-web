import {
  SET_AUTH,
  LOGOUT_AUTH
} from "../actions/types";


export default function (state = null, action) {
  switch (action.type) {
      case SET_AUTH:
          return {
              ...action.payload
          };

      case LOGOUT_AUTH:
          return null;

      default:
          return state;
  }
}