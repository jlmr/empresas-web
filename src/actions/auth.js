import { login } from '../services/EmpresasAPI';
import { SET_AUTH, LOGOUT_AUTH, SET_USER } from './types';
import { getErrors, clearErrors } from './errors';
import { toggleLoading } from './ui';

function setAuth(headers) {
    return {
        type: SET_AUTH,
        payload: {
            "access-token": headers["access-token"],
            "client": headers.client,
            "uid": headers.uid
        }
    }
}

function setUser(payload) {
    return {
        type: SET_USER,
        payload
    }
}

export function authenticateUser(user) {
    return dispatch => {
        dispatch(toggleLoading());
        login(user)
            .then(res => {
                dispatch(setAuth(res.headers));
                dispatch(setUser(res.data));
                dispatch(toggleLoading());
                dispatch(clearErrors());
            })
            .catch(err => {
                let errors = '';
                if(!err || !err.response)
                    errors = ['Error ao realizar requisição!'];
                else
                    errors = err.response.data.errors

                dispatch(getErrors(errors));
                dispatch(toggleLoading());
            });
    };
}