import { GET_ENTERPRISES } from './types';
import { search } from '../services/EmpresasAPI';
import { getErrors, clearErrors } from './errors';

function getEnterprises(payload) {
    return {
        type: GET_ENTERPRISES,
        payload
    };
}

export function searchEnterprises(query, auth) {
    return dispatch => {
        search(query, auth)
            .then(res => {
                dispatch(getEnterprises(res.data.enterprises));
                dispatch(clearErrors());
            })
            .catch(err => {
                let errors = '';
                if(!err || !err.response)
                    errors = ['Error ao realizar requisição!'];
                else
                    errors = err.response.data.errors

                dispatch(getErrors(errors));
            })
    };
}