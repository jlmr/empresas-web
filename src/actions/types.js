export const SET_USER = "SET_USER";
export const LOGOUT_USER = "LOGOUT_USER";

export const SET_AUTH = "SET_AUTH";
export const LOGOUT_AUTH = "LOGOUT_AUTH";

export const GET_ENTERPRISES = "GET_ENTERPRISES";

export const GET_ERRORS = "GET_ERRORS";
export const CLEAR_ERRORS = "CLEAR_ERRORS";

export const TOGGLE_LOADING = "TOGGLE_LOADING";