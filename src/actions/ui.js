import { TOGGLE_LOADING } from './types';

export function toggleLoading() {
    return {
        type: TOGGLE_LOADING
    }
}