import { GET_ERRORS, CLEAR_ERRORS } from './types';

export function getErrors(errors) {
    return {
        type: GET_ERRORS,
        payload: errors
    }
}

export function clearErrors() {
    return {
        type: CLEAR_ERRORS
    }
}