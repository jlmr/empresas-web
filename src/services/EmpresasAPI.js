import axios from 'axios'

const api = 'https://cors-anywhere.herokuapp.com/https://empresas.ioasys.com.br/api/v1'

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
  'X-Requested-With': 'XMLHttpRequest'
}

const securityHeaders = {
  'access-token': null,
  uid: null,
  client: null
}

export const login = body =>
  axios.post(`${api}/users/auth/sign_in`, body, { headers })

export const search = (query, auth) =>
  axios.get(`${api}/enterprises?enterprise_types=1&name=${query}`,
    { headers: { ...headers, ...auth } })
